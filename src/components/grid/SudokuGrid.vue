<script setup lang="ts">
import { ControlMode, type SudokuKey } from "@/data/sudokus";
import { MoveDirection, useSudokuStore } from "@/stores/sudokus";
import { computed, onMounted, onUnmounted, ref, watch } from "vue";
import { useI18n } from "vue-i18n";
import SudokuGridCell from "./SudokuGridCell.vue";
import SudokuThermo from "./SudokuThermo.vue";

const props = withDefaults(
  defineProps<{
    sudokuKey: SudokuKey;
    readonly?: boolean;
  }>(),
  {
    readonly: false,
  }
);

const { t } = useI18n();

const sudokuStore = useSudokuStore();
const sudoku = computed(() => sudokuStore.sudokus[props.sudokuKey]);
let selectedValues: number[] | null = null;
let previousControlMode: ControlMode | null = null;

function changeControlMode(event: KeyboardEvent) {
  if (event.key === "Shift") {
    previousControlMode = sudoku.value.controlMode;
    sudoku.value.controlMode = ControlMode.CornerNote;
  } else if (event.key === "Control") {
    previousControlMode = sudoku.value.controlMode;
    sudoku.value.controlMode = ControlMode.CentralNote;
  }
}

const arrowToMoveDirection: { [id: string]: MoveDirection } = {
  ArrowUp: MoveDirection.Up,
  ArrowLeft: MoveDirection.Left,
  ArrowDown: MoveDirection.Down,
  ArrowRight: MoveDirection.Right,
};

const isFilled = computed(() => {
  for (const row of sudoku.value.grid) {
    for (const cell of row) {
      if (cell.enteredNumber === null && cell.predefinedNumber === null) {
        return false;
      }
    }
  }
  return true;
});

const oldFilled = ref(false);
const showSuccessMessage = ref(false);

watch(isFilled, (value) => {
  if (oldFilled.value !== value) {
    showSuccessMessage.value = value;
  }
});

function handleKey(event: KeyboardEvent) {
  if (props.readonly) return;
  if (event.code.startsWith("Digit")) {
    sudokuStore.enterDigit(props.sudokuKey, parseInt(event.code[5]));
  } else if (event.code === "Backspace" || event.code === "Delete") {
    sudokuStore.deleteSelected(props.sudokuKey);
  } else if (
    previousControlMode !== null &&
    (event.key === "Shift" || event.key === "Control")
  ) {
    sudoku.value.controlMode = previousControlMode;
    previousControlMode = null;
  } else if (event.code.startsWith("Arrow")) {
    if (!event.ctrlKey && !event.shiftKey) {
      sudokuStore.deselectAll(props.sudokuKey);
    }
    sudokuStore.moveSelection(
      props.sudokuKey,
      arrowToMoveDirection[event.code]
    );
  }
}

function* cellCoordinates(): Generator<[number, number]> {
  for (let rowId = 0; rowId < 9; rowId++) {
    for (let colId = 0; colId < 9; colId++) {
      yield [rowId, colId];
    }
  }
}

function getCell(coordinate: [number, number]) {
  return sudokuStore.getCell(props.sudokuKey, coordinate);
}

function startSelectCells(event: MouseEvent | TouchEvent) {
  if (props.readonly) return;
  if (!event.ctrlKey) {
    sudokuStore.deselectAll(props.sudokuKey);
  }
  selectedValues = [];
  mouseOverCell(event);
}

function stopSelectingCells() {
  selectedValues = null;
}

function getCoordinatePartFromElem(elem: Element, coordKey: "x" | "y") {
  const coordPart = elem.getAttribute(coordKey);
  if (coordPart === null) {
    throw new Error(`Could not find key ${coordKey} in ${elem}`);
  }
  return parseInt(coordPart);
}

function getTargetCoordinates(
  event: MouseEvent | TouchEvent
): [number, number] | null {
  const [clientX, clientY] =
    event instanceof MouseEvent
      ? [event.clientX, event.clientY]
      : [event.changedTouches[0].clientX, event.changedTouches[0].clientY];
  let elem = document.elementFromPoint(clientX, clientY);
  while (elem !== null) {
    if (elem.classList.contains("sudoku-grid-cell")) {
      return [
        getCoordinatePartFromElem(elem, "y"),
        getCoordinatePartFromElem(elem, "x"),
      ];
    }
    elem = elem.parentElement;
  }
  return null;
}

function mouseOverCell(event: MouseEvent | TouchEvent) {
  if (selectedValues === null) return;
  const coordinate = getTargetCoordinates(event);
  if (coordinate === null) return;
  const key = coordinate[0] * 10 + coordinate[1];
  if (selectedValues.includes(key)) return;
  selectedValues.push(key);
  sudokuStore.toggleCell(props.sudokuKey, coordinate);
}

onMounted(() => {
  selectedValues = null;
  window.addEventListener("keydown", changeControlMode);
  window.addEventListener("keyup", handleKey);
});

onUnmounted(() => {
  window.removeEventListener("keydown", changeControlMode);
  window.removeEventListener("keyup", handleKey);
});
</script>

<template>
  <div class="grid" tabindex="0">
    <svg class="grid-image" viewBox="-0.025 -0.025 9.05 9.05">
      <SudokuGridCell
        :x="colId"
        :y="rowId"
        width="1"
        height="1"
        class="sudoku-grid-cell"
        :cell="getCell([rowId, colId])"
        @mousedown.prevent="startSelectCells($event)"
        @touchstart.prevent="startSelectCells($event)"
        @mouseup.prevent="stopSelectingCells()"
        @touchend.prevent="stopSelectingCells()"
        @mousemove.prevent="mouseOverCell($event)"
        @touchmove.prevent="mouseOverCell($event)"
        pointer-events="all"
        v-for="[rowId, colId] in cellCoordinates()"
        :key="`${rowId}-${colId}`"
      />
      <g class="non-clickable">
        <SudokuThermo
          width="9"
          height="9"
          :thermo="thermo"
          v-for="(thermo, thermoId) in sudoku.thermos"
          :key="thermoId"
        />
        <line
          :class="['grid-line', { thick: (rowId - 1) % 3 === 0 }]"
          x1="0"
          :y1="rowId - 1"
          x2="9"
          :y2="rowId - 1"
          v-for="rowId in 10"
          :key="rowId"
        ></line>
        <line
          :class="['grid-line', { thick: (colId - 1) % 3 === 0 }]"
          :x1="colId - 1"
          y1="0"
          :x2="colId - 1"
          y2="9"
          v-for="colId in 10"
          :key="colId"
        ></line>
      </g>
    </svg>
    <div :class="{ modal: true, 'is-active': showSuccessMessage }">
      <div class="modal-background" @click="showSuccessMessage = false"></div>
      <div class="modal-card">
        <header class="modal-card-head has-background-success">
          <p class="modal-card-title has-text-white">
            {{ t("success.title") }}
          </p>
          <button
            class="delete"
            aria-label="close"
            @click="showSuccessMessage = false"
          ></button>
        </header>
        <section class="modal-card-body">
          <p>{{ t("success.message") }}</p>
        </section>
      </div>
    </div>
  </div>
</template>

<style lang="scss">
.non-clickable {
  pointer-events: none;
}

.grid {
  user-select: none;
}

.grid-image {
  display: block;
}

.grid-line {
  stroke: black;
  stroke-width: 0.01;
  stroke-linecap: square;

  &.thick {
    stroke-width: 0.05;
  }
}
</style>

<i18n lang="yaml">
de:
  success:
    title: Gratulation!
    message: Du konntest das ganze Sudoku befüllen!

en:
  success:
    title: Congratulations!
    message: You could fill in the entire grid!
</i18n>
