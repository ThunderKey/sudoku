import originalSudokus, {
  ControlMode,
  type SudokuCell,
  type SudokuKey,
  type SudokuState,
} from "@/data/sudokus";
import { cloneDeep } from "lodash";
import { defineStore } from "pinia";

interface State {
  sudokus: { [id in SudokuKey]: SudokuState };
}

function addSorted(initial: number[], value: number) {
  if (initial.includes(value)) return;
  initial.push(value);
  initial.sort((a, b) => a - b);
}

export enum MoveDirection {
  Up,
  Left,
  Down,
  Right,
}

function mod(a: number, n: number) {
  return a - n * Math.floor(a / n);
}

export const useSudokuStore = defineStore("sudokus", {
  persist: true,
  state: (): State => ({ sudokus: cloneDeep(originalSudokus) }),
  getters: {
    visibleSudokus() {
      const visible: Partial<{ [id in SudokuKey]: SudokuState }> = {};
      let key: SudokuKey;
      for (key in this.sudokus) {
        const sudoku = this.sudokus[key];
        if (!sudoku.hidden) {
          visible[key] = sudoku;
        }
      }
      return visible;
    },
    getCell() {
      return (key: SudokuKey, coordinate: [number, number]) =>
        this.sudokus[key].grid[coordinate[0]][coordinate[1]];
    },
  },
  actions: {
    restart(key: SudokuKey) {
      const newSudoku = cloneDeep(originalSudokus[key]);
      newSudoku.showHelp = this.sudokus[key].showHelp;
      this.sudokus[key] = newSudoku;
    },
    deselectAll(key: SudokuKey) {
      this.sudokus[key].grid.forEach((row) =>
        row.forEach((cell) => {
          if (cell.selected) {
            cell.selected = false;
          }
        })
      );
    },
    toggleCell(key: SudokuKey, coordinate: [number, number]) {
      const sudoku = this.sudokus[key];
      const cell = sudoku.grid[coordinate[0]][coordinate[1]];
      cell.selected = !cell.selected;
      sudoku.lastSelected = coordinate;
    },
    moveSelection(key: SudokuKey, direction: MoveDirection) {
      const sudoku = this.sudokus[key];
      if (sudoku.lastSelected === null) return;
      const selection = sudoku.lastSelected;
      switch (direction) {
        case MoveDirection.Up:
          selection[0] = mod(selection[0] - 1, sudoku.grid.length);
          break;
        case MoveDirection.Left:
          selection[1] = mod(selection[1] - 1, sudoku.grid[0].length);
          break;
        case MoveDirection.Down:
          selection[0] = mod(selection[0] + 1, sudoku.grid.length);
          break;
        case MoveDirection.Right:
          selection[1] = mod(selection[1] + 1, sudoku.grid[0].length);
          break;
      }
      if (
        selection[0] < sudoku.grid.length &&
        selection[1] < sudoku.grid[0].length
      ) {
        this.toggleCell(key, selection);
      }
    },
    enterDigit(key: SudokuKey, value: number) {
      const sudoku = this.sudokus[key];
      if (sudoku.controlMode === ControlMode.CornerNote) {
        this.enterCornerNote(key, value);
      } else if (sudoku.controlMode === ControlMode.CentralNote) {
        this.enterCentralNote(key, value);
      } else if (sudoku.controlMode === ControlMode.FullNumber) {
        this.enterRealDigit(key, value);
      } else {
        throw new Error(`unexpected control mode: ${sudoku.controlMode}`);
      }
    },
    enterRealDigit(key: SudokuKey, value: number) {
      const selected = this.selected(key);
      const shouldRemove = selected.every(
        (cell) => cell.enteredNumber == value
      );
      const targetValue = shouldRemove ? null : value;
      selected.forEach((cell) => (cell.enteredNumber = targetValue));
    },
    enterCornerNote(key: SudokuKey, value: number) {
      this.enterToList(key, (cell) => cell.cornerNotes, value);
    },
    enterCentralNote(key: SudokuKey, value: number) {
      this.enterToList(key, (cell) => cell.centralNotes, value);
    },
    enterToList(
      key: SudokuKey,
      listGetter: (cell: SudokuCell) => number[],
      value: number
    ) {
      const lists = this.selected(key).map(listGetter);
      const shouldRemove = lists.every((list) => list.includes(value));
      if (shouldRemove) {
        lists.forEach((list) => {
          const index = list.indexOf(value);
          if (index > -1) {
            list.splice(index, 1);
          }
        });
      } else {
        lists.forEach((list) => addSorted(list, value));
      }
    },
    deleteSelected(key: SudokuKey) {
      this.eachSelected(key, (cell) => {
        cell.enteredNumber = null;
        cell.cornerNotes = [];
        cell.centralNotes = [];
      });
    },
    selected(key: SudokuKey): SudokuCell[] {
      const selected = [];
      for (const row of this.sudokus[key].grid) {
        for (const cell of row) {
          if (!cell.selected) continue;
          selected.push(cell);
        }
      }
      return selected;
    },
    eachSelected(key: SudokuKey, action: (cell: SudokuCell) => void) {
      for (const row of this.sudokus[key].grid) {
        for (const cell of row) {
          if (!cell.selected) continue;
          action(cell);
        }
      }
    },
  },
});
