import { defineStore } from "pinia";

const MAIN_TITLE = "Sudokus @ Keltec";

interface State {
  subTitle: string | null;
}

export const useMetaStore = defineStore("meta", {
  state: (): State => ({ subTitle: null }),
  getters: {
    fullTitle(): string {
      if (this.subTitle === null) {
        return MAIN_TITLE;
      }
      return `${this.subTitle} | ${MAIN_TITLE}`;
    },
  },
  actions: {
    setSubTitle(subTitle: string | null) {
      this.subTitle = subTitle;
    },
  },
});
