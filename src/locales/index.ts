import type { I18nOptions } from "vue-i18n";

const availableLocales = ["de", "en"];

function detectLanguage() {
  const languages = navigator.languages || [navigator.language];
  for (const language of languages) {
    const shortLanguage = language.split("-")[0];
    if (availableLocales.includes(shortLanguage)) {
      return shortLanguage;
    }
  }
  return availableLocales[0];
}

export default <I18nOptions>{
  legacy: false,
  locale: detectLanguage(),
  messages: Object.assign(
    {},
    ...availableLocales.map((locale) => ({ [locale]: {} }))
  ),
};
