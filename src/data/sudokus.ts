export type Tuple<T, N, R extends T[] = []> = R["length"] extends N
  ? R
  : Tuple<T, N, [...R, T]>;

function map9<T>(func: (i: number) => T): Tuple<T, 9> {
  return Array(9)
    .fill(undefined)
    .map((_value, i) => func(i)) as Tuple<T, 9>;
}

function map9x9<T>(func: (x: number, y: number) => T): Tuple<Tuple<T, 9>, 9> {
  return map9((x) => map9((y) => func(x, y)));
}

export enum CellColor {
  GREEN = "green",
}

export interface SudokuCell {
  predefinedNumber: number | null;
  enteredNumber: number | null;
  selected: boolean;
  color: CellColor | null;
  cornerNotes: number[];
  centralNotes: number[];
}

export type SudokuGrid = Tuple<Tuple<SudokuCell, 9>, 9>;

export enum ControlMode {
  FullNumber,
  CornerNote,
  CentralNote,
}

export interface SudokuState {
  helpTranslations: string[];
  hidden: boolean;
  showHelp: boolean;
  grid: SudokuGrid;
  thermos: Array<Array<[number, number]>>;
  controlMode: ControlMode;
  lastSelected: [number, number] | null;
}

function buildEmptyState(
  helpTranslations: string[],
  hidden = false
): SudokuState {
  return {
    helpTranslations: helpTranslations,
    hidden,
    showHelp: true,
    grid: map9x9(
      (): SudokuCell => ({
        predefinedNumber: null,
        enteredNumber: null,
        selected: false,
        color: null,
        cornerNotes: [],
        centralNotes: [],
      })
    ),
    thermos: [],
    controlMode: ControlMode.FullNumber,
    lastSelected: null,
  };
}

function buildSaveTheDate(): SudokuState {
  const state = buildEmptyState(["normal"]);
  state.grid[0][0].predefinedNumber = 1;
  state.grid[0][6].predefinedNumber = 3;
  state.grid[0][7].predefinedNumber = 7;
  state.grid[1][1].predefinedNumber = 2;
  state.grid[1][2].predefinedNumber = 9;
  state.grid[1][4].predefinedNumber = 5;
  state.grid[1][6].predefinedNumber = 6;
  state.grid[1][7].predefinedNumber = 8;
  state.grid[1][8].predefinedNumber = 4;
  state.grid[2][2].predefinedNumber = 3;
  state.grid[2][3].predefinedNumber = 7;
  state.grid[2][4].predefinedNumber = 4;
  state.grid[2][6].predefinedNumber = 9;
  state.grid[3][1].predefinedNumber = 7;
  state.grid[3][2].predefinedNumber = 2;
  state.grid[3][4].predefinedNumber = 1;
  state.grid[3][6].predefinedNumber = 4;
  state.grid[3][7].predefinedNumber = 9;
  state.grid[4][2].color = CellColor.GREEN;
  state.grid[4][4].color = CellColor.GREEN;
  state.grid[4][6].color = CellColor.GREEN;
  state.grid[4][7].color = CellColor.GREEN;
  state.grid[5][0].predefinedNumber = 6;
  state.grid[5][5].predefinedNumber = 8;
  state.grid[6][0].predefinedNumber = 4;
  state.grid[6][2].predefinedNumber = 6;
  state.grid[6][4].predefinedNumber = 8;
  state.grid[6][6].predefinedNumber = 7;
  state.grid[6][8].predefinedNumber = 3;
  state.grid[7][1].predefinedNumber = 5;
  state.grid[7][3].predefinedNumber = 3;
  state.grid[7][5].predefinedNumber = 7;
  state.grid[8][0].predefinedNumber = 9;
  state.grid[8][4].predefinedNumber = 2;
  state.grid[8][5].predefinedNumber = 1;
  state.grid[8][8].predefinedNumber = 5;
  return state;
}

function buildWeddingInvitation() {
  const state = buildEmptyState(["normal", "thermo"]);
  state.thermos = [
    // heart left
    [
      [5, 3],
      [2, 0],
      [0, 2],
      [1, 3],
    ],
    // heart right
    [
      [6, 4],
      [2, 8],
      [0, 6],
      [2, 4],
    ],
    // S top
    [
      [7, 1],
      [7, 0],
      [6, 0],
      [6, 2],
    ],
    // S bottom
    [
      [8, 0],
      [8, 2],
      [7, 2],
    ],
    // N
    [
      [8, 6],
      [6, 6],
      [8, 8],
      [6, 8],
    ],
    // fix bottom1
    [
      [7, 3],
      [7, 5],
    ],
    // fix bottom2
    [
      [8, 3],
      [8, 5],
    ],
    // fix top
    [
      [0, 0],
      [0, 1],
    ],
  ];
  return state;
}

function buildThermoExample() {
  const state = buildEmptyState(["normal", "thermo"], true);
  // example 1
  state.grid[0][0].predefinedNumber = 2;
  state.grid[1][0].predefinedNumber = 3;
  state.grid[2][0].predefinedNumber = 5;
  state.grid[3][0].predefinedNumber = 8;
  // example 2
  state.grid[2][7].predefinedNumber = 1;
  state.grid[3][6].predefinedNumber = 2;
  state.grid[4][5].predefinedNumber = 7;
  state.grid[3][5].predefinedNumber = 9;
  // example 2
  state.grid[8][3].predefinedNumber = 1;
  state.grid[7][3].predefinedNumber = 5;
  state.grid[6][3].predefinedNumber = 9;
  state.thermos = [
    // example 1
    [
      [0, 0],
      [1, 0],
      [2, 0],
      [3, 0],
    ],
    // example 2
    [
      [2, 7],
      [3, 6],
      [4, 5],
      [3, 5],
    ],
    // example 3
    [
      [8, 3],
      [7, 3],
      [6, 3],
    ],
  ];
  return state;
}

const sudokus = {
  "save-the-date": buildSaveTheDate(),
  "wedding-invitation": buildWeddingInvitation(),
  "thermo-example": buildThermoExample(),
};

export type SudokuKey = keyof typeof sudokus;

export default sudokus;
