import { createPinia } from "pinia";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
import { createApp } from "vue";
import { createI18n } from "vue-i18n";

import Unicon from "vue-unicons";
// @ts-ignore
import * as icons from "vue-unicons/dist/icons";

import App from "./App.vue";
import locales from "./locales";
import router from "./router";

import "./assets/main.scss";

const i18n = createI18n(locales);

const app = createApp(App);

Unicon.add([icons.uniCancel, icons.uniQuestionCircle, icons.uniRedo]);

app.use(i18n);

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);
app.use(pinia);

app.use(router);
// @ts-ignore
app.use(Unicon);

app.mount("#app");
