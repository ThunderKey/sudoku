import SudokuGridCell from "@/components/grid/SudokuGridCell.vue";
import { CellColor, type SudokuCell } from "@/data/sudokus";
import { createTestingPinia } from "@pinia/testing";
import { mount } from "@vue/test-utils";
import { expect, test, vi } from "vitest";

vi.mock("vue-router", () => ({
  resolve: vi.fn(),
}));

test.each([
  [{ predefinedNumber: 1 }, "1"],
  [{ predefinedNumber: 5, cornerNotes: [4], centralNotes: [9] }, "5"],
  [{ enteredNumber: 8 }, "8"],
  [{ enteredNumber: 5, cornerNotes: [4], centralNotes: [9] }, "5"],
  [{ cornerNotes: [4, 7, 9] }, "479"],
  [{ centralNotes: [2, 3, 7] }, "237"],
])(
  "text of sudoku grid cell for %s",
  (cellData: Partial<SudokuCell>, expected: string) => {
    const cell: SudokuCell = {
      predefinedNumber: null,
      enteredNumber: null,
      selected: false,
      color: CellColor.GREEN,
      cornerNotes: [],
      centralNotes: [],
      ...cellData,
    };

    const wrapper = mount(SudokuGridCell, {
      props: { cell },
      global: {
        plugins: [createTestingPinia()],
      },
    });

    expect(wrapper.text()).toBe(expected);
  }
);
