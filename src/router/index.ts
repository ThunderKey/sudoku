import HomeView from "@/views/HomeView.vue";
import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  linkActiveClass: "is-active",
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/sudoku/:sudokuKey",
      name: "sudoku",
      component: () => import("@/views/SudokuView.vue"),
    },
  ],
});

export default router;
