#!/bin/sh

set -e

[ -z "$PORT" ] && PORT=80
yarn install
exec yarn run dev --port "$PORT" --host
