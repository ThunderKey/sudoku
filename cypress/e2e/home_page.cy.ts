// https://docs.cypress.io/api/introduction/api.html

describe("Home Page", () => {
  it("visits the app root url", () => {
    cy.visit("/");
    cy.get("h1").should(
      "have.text",
      "This is a collection of playable sudokus"
    );

    cy.contains("nav a", "Save the Date").click();
    cy.get("h1").should("have.text", "Save the Date");

    cy.contains("nav a", "Hochzeits Einladung").click();
    cy.get("h1").should("have.text", "Hochzeits Einladung");
  });
});
